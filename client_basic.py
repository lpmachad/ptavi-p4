#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto

    SERVER = sys.argv[1]    # coge el primer argumento que es la IP del servidor
    PORT = int(sys.argv[2])    # coge el segundo argumento que es el puerto y lo pasamos a int porque es un numero
    LINE = ' '.join(sys.argv[3:])    # cogemos a partir del tercer argumento

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
