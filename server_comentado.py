#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys    # coger las posiciones de la linea de comandos

# Constantes. Puerto.
PORT = 6001


class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """

    def handle(self):    # El manejador, siempre tiene que tenerlo el server socket
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]    # datos recibidos
        sock = self.request[1]    # es la ip y el puerto por el que se puede responder al cliente
        received = data.decode('utf-8')    # datos recibidos decodificados del cliente
        print(f"El cliente nos envía: {received}.")    # imprime los datos que nos envia el cliente
        sock.sendto("Hemos recibido tu petición".encode('utf-8'), self.client_address)
        # envia el mensaje al cliente a client_address
        # dentro de client_adress ya viene la IP y el PORT


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), EchoHandler)    # abre el servidor UDP, con el local host, el puerto
        # y lo que hemos capturado lo opera handle
        print(f"Lanzando servidor UDP de eco ({PORT})...")    # imprime el puerto que abre el socket
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")    # saca el error

    try:
        serv.serve_forever()    # el servidor queda activo hasta que escuche algo
    except KeyboardInterrupt:    # expeto si se interrumpe el servidor
        print("Finalizado servidor")    # se cierra
        sys.exit(0)    # el sistema acaba y termina el programa


if __name__ == "__main__":
    main()
