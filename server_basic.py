#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        ip = self.client_address[0]    # ip del cliente
        client_port = self.client_address[1]    # puerto del cliente
        received = data.decode('utf-8')
        print(f"{ip} {client_port} {received}")
        sock.sendto(received.upper().encode('utf-8'), self.client_address)    # Con upper se pone en mayusculas


def main():
    PORT = int(sys.argv[1])

    try:
        serv = socketserver.UDPServer(('', PORT), EchoHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
