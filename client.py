#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
LINE = 'Hola'


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))    # Envia el mensaje el servidor
            data = my_socket.recv(1024)    # Lo que recibe el cliente
            print('Recibido:', data.decode('utf-8'))    # Imprime lo que recibe del servidor

            my_socket.sendto(data, (SERVER, PORT))    # Envia el cliente el mensaje que ha recibido del servidor
            data2 = my_socket.recv(1024)    # Recibe el cliente del servidor
            print('Recibido:', data2.decode('utf-8'))    # Imprime de nuevo lo que recibe del servidor
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
