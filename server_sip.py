#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time


class SIPRequest:

    def __init__(self, data):
        self.data = data

    def parse(self):
        received = self.data.decode('utf-8')
        self._parse_command(received)
        primer_nl = received.split('\n')[1:]
        self._parse_headers(primer_nl[0])

    def _get_address(self, uri):  # cortar el mensaje para dividir correo y sip
        cut = uri.split(":")
        address = cut[1]
        schema = cut[0]
        return address, schema

    def _parse_command(self, line):
        cut = line.split(' ')
        self.command = cut[0].upper()
        self.uri = cut[1]
        sip_uri = self._get_address(self.uri)
        self.address = sip_uri[0]
        schema = sip_uri[1]
        if self.command == "REGISTER":
            if schema == "sip":
                self.result = "200 OK"
            else:
                self.result = "416 Unsupported URI Scheme"
        else:
            self.result = "405 Method Not Allowed"

    def _parse_headers(self, first_nl):
        split_line = first_nl.split(':')
        value = split_line[1]
        self.headers = value


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """
    registro = {}
    lista_json = []

    def registered2json(self, cliente):
        with open("registered.json", "w") as file:  # escribir archivo json
            json.dump(cliente, file, indent=1)
        file.close()

    def json2registered(self):
        try:
            with open("registered.json", "r") as openfile:  # leer archivo json
                self.lista_json = json.load(openfile)
        except FileNotFoundError:  # si no existe el archivo, lo crea
            with open("registered.json", "w"):  # crear archivo json
                pass

    def process_register(self, elemento):  # registra los cambios

        if int(elemento.headers) > 0:
            fecha = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
            time_expire = f"{fecha} + {int(elemento.headers)}"
            self.registro[elemento.address] = elemento.headers
            client = {"address": self.client_address[0], "expires": time_expire}
            # añadimos a la lista
            self.lista_json.append(elemento.address)  # añadimos por separado
            self.lista_json.append(client)
            self.registered2json(self.lista_json)

            print(f'\r\nDatos añadidos en registered.json')
            print(f'Pulse Control C para salir\r\n')

        if int(elemento.headers) == 0:
            del self.registro[elemento.address]
            self.json2registered()
            index = self.lista_json.index(elemento.address)
            self.lista_json.remove(self.lista_json[int(index)])
            self.lista_json.remove(
                self.lista_json[int(index)])  # eliminamos los dos elementos correspondientes al address y el usuario

        self.registered2json(self.lista_json)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]  # datos que recibo
        sock = self.request[1]  # socket que puedo usar para responder el cual ya contiene la direccion del cliente
        sip_request = SIPRequest(data)
        sip_request.parse()

        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)


def main():
    port = sys.argv[1]

    if len(sys.argv) < 2:
        sys.exit("Usage: python3 server.py <port>")
    try:
        # creacion del servidor que cuando reciba mensajes llamara al manejador
        serv = socketserver.UDPServer(('', int(port)), SIPRegisterHandler)  # se puede cambiar al int arriba
        # la cadena vacia '' son todas mis direcciones IP y el puerto especificado
        # el EchoHandler sera el manejador del mensaje
        print(f"Server listening in port ({port})")
    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}.")

    try:
        serv.serve_forever()  # escucha siempre hasta cerrarlo
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
