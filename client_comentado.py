#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket    # Abrir un canal de servidor cliente

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
LINE = '¡Hola mundo!'


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:    # Asi se realiza un socket UDP
            print(f"Enviando a {SERVER}:{PORT}:", LINE)
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))    # Convertimos en bytes lo que enviamos
            # Segundo le decimos donde lo enviamos, la IP y el Puerto
            # Primero que enviamos, segundo a donde en forma de tupla
            data = my_socket.recv(1024)    # La memoria que reservamos para lo que vamos a enviar 1024
            print('Recibido: ', data.decode('utf-8'))    # Decodificamos data
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
